<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('stations/{station}', 'StationController@showSpecificStation');
Route::get('stations', 'StationController@showAllStation');

Route::post('/register', 'UserController@store');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['auth:api', 'owner:stations']], function(){

    Route::delete('stations/{stations}', 'StationController@destroy');
    Route::put('stations/{stations}', 'stationController@update');
    Route::post('stations/{stations}/measures', 'MeasureController@store');
});

Route::post('stations', 'StationController@store');
Route::get('stations/{stations}/measures/', 'MeasureController@showStationMeasures');
Route::get('stations/{stations}/measures/24h', 'MeasureController@show24h');
Route::get('stations/{stations}/measures/iqa', 'MeasureController@showLastValue');


