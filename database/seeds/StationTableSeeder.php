<?php

use Illuminate\Database\Seeder;
use App\Station;

class StationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $station = new Station();
        $station->name = 'Charny_13';
        $station->lat = 46.850872;
        $station->long = -71.351315;
        $station->user_id = 1;
        $station->save();

        $station = new Station();
        $station->name = 'Des_chatêls_24';
        $station->lat = 46.843314;
        $station->long = -71.388499;
        $station->user_id = 2;
        $station->save();

        $station = new Station();
        $station->name = 'Lévis_83';
        $station->lat = 46.714831;
        $station->long = -71.189001;
        $station->user_id = 3;
        $station->save();

        $station = new Station();
        $station->name = 'Ste_Foy_48';
        $station->lat = 46.786762;
        $station->long = -71.282209;
        $station->user_id = 1;
        $station->save();

        $station = new Station();
        $station->name = 'Saint_Nicolas_31';
        $station->lat = 46.723099;
        $station->long = -71.295320;
        $station->user_id = 2;
        $station->save();

        $station = new Station();
        $station->name = 'Pont_Rouge_37';
        $station->lat = 46.751534;
        $station->long = 71.695971;
        $station->user_id = 3;
        $station->save();

        $station = new Station();
        $station->name = 'Wendake_70';
        $station->lat = 46.868394;
        $station->long = -71.365774;
        $station->user_id = 1;
        $station->save();
    }
}
