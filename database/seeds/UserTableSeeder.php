<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            array(
                'name' => 'admin',
                'email' => 'admin@email.quebec',
                'password' => Hash::make('admin'),
            )
        );

        User::create(
            array(
                'name' => 'Tech info',
                'email' => 'tech@email.quebec',
                'password' => Hash::make('tech'),
            )
        );

        User::create(
            array(
                'name' => 'tester',
                'email' => 'tech@test.test',
                'password' => Hash::make('tech'),
            )
        );
    }
}
