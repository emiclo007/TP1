<?php

use Illuminate\Database\Seeder;
use App\Measure;

class MeasureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $measure = new Measure();
        $measure->value = 2;
        $measure->description = 'CO2';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 3;
        $measure->description = 'CO2';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 4;
        $measure->description = 'CO2';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 4;
        $measure->description = 'NH3';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 3;
        $measure->description = 'NH3';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 5;
        $measure->description = 'NH3';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 25;
        $measure->description = 'H20';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 12;
        $measure->description = 'H20';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 13;
        $measure->description = 'H20';
        $measure->station_id = 1;
        $measure->save();

        $measure = new Measure();
        $measure->value = 20;
        $measure->description = 'NO2';
        $measure->station_id = 2;
        $measure->save();

        $measure = new Measure();
        $measure->value = 12;
        $measure->description = 'CH4';
        $measure->station_id = 2;
        $measure->save();

        $measure = new Measure();
        $measure->value = 29;
        $measure->description = 'O2';
        $measure->station_id = 2;
        $measure->save();

        $measure = new Measure();
        $measure->value = 0;
        $measure->description = 'Ne';
        $measure->station_id = 3;
        $measure->save();

        $measure = new Measure();
        $measure->value = 12;
        $measure->description = 'H2O';
        $measure->station_id = 3;
        $measure->save();

        $measure = new Measure();
        $measure->value = 1;
        $measure->description = 'NH3';
        $measure->station_id = 3;
        $measure->save();

        $measure = new Measure();
        $measure->value = 22;
        $measure->description = 'Ne';
        $measure->station_id = 4;
        $measure->save();

        $measure = new Measure();
        $measure->value = 7;
        $measure->description = 'O2';
        $measure->station_id = 4;
        $measure->save();

        $measure = new Measure();
        $measure->value = 7;
        $measure->description = 'I2';
        $measure->station_id = 4;
        $measure->save();

        $measure = new Measure();
        $measure->value = 4;
        $measure->description = 'Ne';
        $measure->station_id = 5;
        $measure->save();

        $measure = new Measure();
        $measure->value = 19;
        $measure->description = 'I2';
        $measure->station_id = 5;
        $measure->save();

        $measure = new Measure();
        $measure->value = 20;
        $measure->description = 'Kr';
        $measure->station_id = 5;
        $measure->save();

        $measure = new Measure();
        $measure->value = 21;
        $measure->description = 'CH4';
        $measure->station_id = 6;
        $measure->save();

        $measure = new Measure();
        $measure->value = 2;
        $measure->description = 'CH4';
        $measure->station_id = 6;
        $measure->save();

        $measure = new Measure();
        $measure->value = 3;
        $measure->description = 'CO';
        $measure->station_id = 6;
        $measure->save();

        $measure = new Measure();
        $measure->value = 4;
        $measure->description = 'Xe';
        $measure->station_id = 6;
        $measure->save();

        $measure = new Measure();
        $measure->value = 25;
        $measure->description = 'CO2';
        $measure->station_id = 7;
        $measure->save();

        $measure = new Measure();
        $measure->value = 25;
        $measure->description = 'Ne';
        $measure->station_id = 7;
        $measure->save();

        $measure = new Measure();
        $measure->value = 14;
        $measure->description = 'Ar';
        $measure->station_id = 7;
        $measure->save();
    }
}
