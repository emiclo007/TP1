<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
class MeasureTest extends TestCase
{
    public function test_posting_measure_with_authentification()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'value' => 22,
            'description' => 'CO2',
        ];

        $response = $this->json('POST', '/api/stations/1/measures', $data);

        $response->assertStatus(201);
    }

    public function test_posting_measure_without_authentification()
    {
        $data = [
            'value' => 22,
            'description' => 'CO2',
        ];

        $response = $this->json('POST', '/api/stations/1/measures', $data);

        $response->assertStatus(401);
    }

    public function test_posting_measure_without_value()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'value' => null,
            'description' => 'CO2',
        ];

        $response = $this->json('POST', '/api/stations/1/measures', $data);

        $response->assertStatus(422);
    }

    public function test_posting_measure_without_description()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'value' => 22,
            'description' => null,
        ];

        $response = $this->json('POST', '/api/stations/1/measures', $data);

        $response->assertStatus(422);
    }

    public function test_getting_all_measures_from_a_station()
    {
        $response = $this->json('GET', '/api/stations/1/measures');

        $response->assertStatus(200);
    }

    public function test_getting_all_measures_from_a_non_existing_station()
    {
        $response = $this->json('GET', '/api/stations/58/measures');

        $response->assertStatus(404);
    }

    public function test_getting_measures_from_a_station_24_hours()
    {
        $response = $this->json('GET', '/api/stations/1/measures/24h');

        $response->assertStatus(200);
    }

    public function test_getting_measures_from_a_non_existing_station_24_hours()
    {
        $response = $this->json('GET', '/api/stations/58/measures/24h');

        $response->assertStatus(404);
    }

    public function test_getting_latest_measures_from_a_station()
    {
        $response = $this->json('GET', '/api/stations/1/measures/iqa');

        $response->assertStatus(200);
    }

    public function test_getting_latest_measures_from_a_non_existing_station()
    {
        $response = $this->json('GET', '/api/stations/58/measures/iqa');

        $response->assertStatus(404);
    }
}
