<?php

namespace Tests\Feature;

use Tests\TestCase;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_Register_With_Duplicated_Email_Date()
    {
        $data = [
           'email' => 'test@hotmail.com',
           'name' => 'test1',
           'password' => 'secretpassword',
        ];

        $response = $this->json('POST', '/api/register', $data);

        $response = $this->json('POST', '/api/register', $data);

        $response->assertStatus(422);
    }

    public function test_Register_without_email_test()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'email' => null,
            'name' => 'test1',
            'password' => 'secretpassword',
        ];

        $response = $this->json('POST', '/api/register', $data);

        $response->assertStatus(422);
    }

    public function test_Register_Without_name_Test()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'email' => 'jojo@bell.net',
            'name' => null,
            'password' => 'secretpassword',
        ];

        $response = $this->json('POST', '/api/register', $data);

        $response->assertStatus(422);
    }

    public function test_Register_Without_Password_Test()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'email' => 'jojo@bell.net',
            'name' => 'test1',
            'password' => null,
        ];

        $response = $this->json('POST', '/api/register', $data);

        $response->assertStatus(422);
    }

    public function test_Register_with_invalid_email_test()
    {
        Passport::actingAs(\App\User::find(1));

        $data = [
            'email' => 'COURRIEL_INVALIDE',
            'name' => 'test1',
        ];

        $response = $this->json('POST', '/api/register', $data);

        $response->assertStatus(422);
    }
}
