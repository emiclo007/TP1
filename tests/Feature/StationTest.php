<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use App\user;

class StationTest extends TestCase
{
    public function test_station_test()
    {
        $response = $this->json('GET', '/api/stations/');
        $response->assertStatus(200);
    }

    public function test_specific_station_test()
    {
        $response = $this->json('GET', '/api/stations/1');
        $response->assertStatus(200);
    }

    public function test_station_that_does_not_exist()
    {
        $response = $this->json('GET', '/api/stations/20');
        $response->assertStatus(404);
    }

    public function test_put_on_specific_station_with_authorization()
    {
        Passport::actingAs(User::find(1));

        $data = [
            'name' => 'yoyo',
            'lat' => 34.5,
            'long' => 23.3
        ];

        $response = $this->json('PUT', '/api/stations/1', $data);
        $response->assertStatus(200);
    }

    public function test_put_on_specific_station_without_authorization()
    {
        $data = [
            'name' => 'yoyo',
            'lat' => 34.5,
            'long' => 23.3
        ];

        $response = $this->json('PUT', '/api/stations/1', $data);
        $response->assertStatus(401);
    }

    public function test_create_new_station()
    {
        $data = [
            'name' => 'yoyo',
            'lat' => 34.5,
            'long' => 23.3,
            'user_id' => 2
        ];

        $response = $this->json('POST', '/api/stations', $data);
        $response->assertStatus(201);
    }

    public function test_create_new_station_without_name()
    {
        $data = [
            'name' => null,
            'lat' => 34.5,
            'long' => 23.3,
            'user_id' => 2
        ];

        $response = $this->json('POST', '/api/stations', $data);
        $response->assertStatus(422);
    }

    public function test_create_new_station_without_lat()
    {
        $data = [
            'name' => 'yoyo',
            'lat' => null,
            'long' => 23.3,
            'user_id' => 2
        ];

        $response = $this->json('POST', '/api/stations', $data);
        $response->assertStatus(422);
    }

    public function test_create_new_station_without_long()
    {
        $data = [
            'name' => 'yoyo',
            'lat' => 36.6,
            'long' => null,
            'user_id' => 2
        ];

        $response = $this->json('POST', '/api/stations', $data);
        $response->assertStatus(422);
    }

    public function test_create_new_station_without_user_id()
    {
        $data = [
            'name' => 'yoyo',
            'lat' => 36.6,
            'long' => 23.8,
            'user_id' => null
        ];

        $response = $this->json('POST', '/api/stations', $data);
        $response->assertStatus(422);
    }

    public function test_delete_specific_station_without_authorization()
    {
        $response = $this->json('DELETE', '/api/stations/1');
        $response->assertStatus(401);
    }

    public function test_delete_specific_station_with_authorization()
    {
        Passport::actingAs(User::find(1));

        $response = $this->json('DELETE', '/api/stations/1');
        $response->assertStatus(200);
    }
}
