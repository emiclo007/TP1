<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    protected $fillable = ['description', 'value', 'station_id'];

    protected $appends = ['indice'];

    protected $hidden = ['station_id', 'update_at'];

    public function station()
    {
        return $this->belongsTo('App\Station');
    }

    protected function getIndiceAttribute()
    {
        $color = 'Vert';
        $label = 'Bon';

        if($this->value >= 10 && $this->value < 20)
        {
            $color = 'Jaune';
            $label = 'Modéré';
        }

        elseif ($this->value >= 20 && $this->value < 30) {
            $color = 'Rouge';
            $label = 'Mauvais';
        }

        return ['color'=>$color, 'label'=>$label];
    }
}
