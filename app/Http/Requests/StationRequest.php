<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StationRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return[
            'name' => 'required',
            'lat' => 'required',
            'long' => 'required',
            'user_id' => 'required'
        ];
    }
}