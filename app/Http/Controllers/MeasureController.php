<?php

namespace App\Http\Controllers;

use App\Measure;
use App\Station;
use Illuminate\Http\Request;
use App\Http\Resources\MeasureResource as MeasureResource;
use App\Http\Requests\MeasureRequest;
use Illuminate\Support\Facades\DB;

class MeasureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measure = DB::table('measures')->paginate(10);
        return MeasureResource::collection($measure);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show24h(Station $stations)
    {
        $last24hours = $stations->get24h();
        return response($last24hours)->setStatusCode(200);
    }

    public function showLastValue(Station $stations)
    {
        $lastValue = $stations->getLastMeasures($stations);
        return response($lastValue)->setStatusCode(200);
    }

    public function showStationMeasures(Station $stations)
    {
        $measures = $stations->getStationMeasures($stations);
        return response($measures)->setStatusCode(200);
    }

    public function store(MeasureRequest $request, Station $stations)
    {
        $measure = new Measure();
        $measure->value = $request->value;
        $measure->description = $request->description;
        $measure->created_at = $request->created_at;
        $measure->station_id = $stations->id;
        $measure->save();

        return response($measure)->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function show(Measure $measure)
    {
        return new MeasureResource($measure);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Measure $measure, Station $stations)
    {
        $measure->value = $request->value;
        $measure->description = $request->description;
        $measure->created_at = $request->created_at;
        $measure->station_id = $stations->station_id;
        $measure->save();

        return $measure;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Measure  $measure
     * @return \Illuminate\Http\Response
     */
    public function destroy(Measure $measure)
    {
        $measure->delete();
        return $measure;
    }
}
