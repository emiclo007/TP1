<?php

namespace App\Http\Controllers;

use App\Http\Requests\StationRequest;
use App\Station;
use App\Http\Resources\StationResource as StationResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StationController extends Controller
{

    public function index()
    {
        $station = DB::table('station')->paginate(10);
        return StationResource::collection($station);
    }

    public function store(StationRequest $request)
    {
        $station = new Station();
        $station->name = $request->input('name');
        $station->lat = $request->input('lat');
        $station->long = $request->input('long');
        $station->user_id = $request->input('user_id');
        $station->save();

        return response($station)->setStatusCode(201);
    }

    public function showSpecificStation(Station $station)
    {
        return Station::find($station->id);
    }

    public function showAllStation()
    {
        return Station::orderBy('created_at', 'desc')->paginate(10);
    }

    public function update(Request $request, Station $stations)
    {
        $stations->name = $request->input('name');
        $stations->lat = $request->input('lat');
        $stations->long = $request->input('long');
        $stations->save();

        return $stations;
    }

    public function destroy(Station $stations)
    {
        $stations->delete();
        return $stations;
    }
}
