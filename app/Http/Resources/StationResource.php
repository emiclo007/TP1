<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class StationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'lat' => $this->lat,
            'long' => $this->long,
            'id' => $this->id,
            'user_id' => $this->user_id
        ];
    }
}
