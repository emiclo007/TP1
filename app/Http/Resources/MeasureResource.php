<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MeasureResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'description' => $this->description,
            'value' => $this->value,
            'station_id' => $this->station_id,
            'created_at' => $this->created_at,
            'indice' => $this->getIndiceAttribute()
        ];
    }
}
