<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Station extends Model
{

    protected $fillable = ['name', 'lat', 'long'];

    public function measures()
    {
        return $this->hasMany('App\Measure');
    }
    public function get24h()
    {
        $fromDate = Carbon::now()->subHours(24);
        $toDate = new carbon('now');

        $measuresCollection =
            $this->measures()
                ->whereBetween('created_at',
                    array($fromDate->toDateString(),
                        $toDate->toDateTimeString()))->get();

        $measuresGrouped = $measuresCollection->groupBy('description');

        return $measuresGrouped;
    }

    public function getLastMeasures(Station $stations)
    {
        $res = \App\Station::find($stations->id)
            ->measures()
            ->orderBy('created_at', 'desc')
            ->groupBy('description')
            ->get();
        $measuresCollection = array();
        foreach($res as $item)
        {
            $ms = new Measure();
            $ms->value = $item->value;
            $ms->description = $item->description;
            $ms->created_at = $item->created_at;

            $measuresCollection[] = $ms;
        }

        return $measuresCollection;
    }

    public function getStationMeasures(Station $stations)
    {
        $res[] = \App\Station::find($stations->id)->measures()->orderBy('created_at', 'desc')->paginate(24);
        return $res;
    }
}
